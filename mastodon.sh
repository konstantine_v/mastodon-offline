#!/bin/bash

# Mostodon Offline-Focused Command-Line Script
# Customize to your likeing
# Docs: https://docs.joinmastodon.org/methods/timelines/

# Defaults:
# Instance will default to mastodon.social
# Timeline will default to federated
# Postlimit will default to 10

# Example:
# mastodon-update -i linuxrocks.online -p 50 -l

# Specify country in the `-c` flag
while getopts i:l:p: option
do
case "${option}"
in
i) INSTANCE=${OPTARG};;
l) TIMELINE="&local=true";;
p) POSTS_LIMIT=${OPTARG};;
esac
done

# Default to last 10 posts
if [[ -z "$POSTS_LIMIT" ]]; then
POSTS_LIMIT=10
fi

# If instance flag was set then save to config cache file
# If no instance is specified then check cache and if nothing then set to default
if [[ "$INSTANCE" ]]; then
echo $INSTANCE > $HOME/.cache/mastodon_config
else
    if [[ -f $HOME/.cache/mastodon_instance ]]; then
        INSTANCE=$(cat $HOME/.cache/mastodon_config)
    else
        INSTANCE="mastodon.social"
    fi
fi

# CURL info needed from API
read -r -p "Download the latest from your mastodon timeline? [y/N] " response
case "$response" in
[yY][eE][sS]|[yY])
curl https://$INSTANCE/api/v1/timelines/public?limit=$POSTS_LIMIT$TIMELINE > $HOME/.cache/mastodon_timeline ;;
*) ;;
esac

# Colours
RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\034[0;31m'
NC='\033[0m'

# Use JQ to parse JSON for readability
echo -e "${RED}Your Mastodon Timeline${NC}\n" > $HOME/.cache/mastodon_timeline_formatted
for row in $(cat $HOME/.cache/mastodon_timeline | jq -r '.[] | @base64'); do
    _jq() {
     echo ${row} | base64 --decode | jq -r ${1}
    }
   echo -e "${BLUE}Username:${NC} $(_jq '.account.username')\n$(_jq '.content')\n======" >> $HOME/.cache/mastodon_timeline_formatted
done

# TODO
# Parse HTML from Account Toots
